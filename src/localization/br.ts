export const br = {
	navbar: {
		englishButton: {
			ariaLabel: "Trocar para o Inglês.",
		},
		portugueseButton: {
			ariaLabel: "Trocar para o Português (Idioma atual).",
		},
	},
	sections: {
		welcome: {
			headingText: [
				"Olá, eu sou Luan!",
				"Iaê, aqui é o Luan.",
				"Fala ae galerinha, beleza? Aqui quem fala é o Luan Dev'plays",
			],
			paragraphs: [
				"Sou desenvolvedor full-stack desde 2018, comecei atuando com PHP em projetos pessoais, depois entrei como estágio, mas foi migrando pro TypeScript em 2019 que eu me apaixonei e utilizo essa  stack até hoje.",
				"Mas então, o que mais você gostaria de saber sobre minha pessoa?",
			],
		},
		aboutMe: {
			headingText: [
				"Então, falando um pouco sobre mim...",
				"Já que insiste, puxa uma cadeira ai.",
			],
			paragraphs: [
				"Então, me chamo Luan Oliveira Silva, tenho {years} anos, nascido e atualmente morando em Salvador-BA, tenho 3 gatinhos e amo games.",
			]
		},
		jobs: {
			headingText: [
				"Sobre o que eu ja fiz até hoje.",
				"Alguns dos frutos da minha carreira profissional.",
			],
			paragraphs: [
				"Ao longo da minha carreira de desenvolvedor, fiz alguns projetos pessoais e profissionais.",
			]
		},
		experience: {
			headingText: [
				"Minha carreira como Dev.",
				"O caminho da minha jornada profissional.",
			],
			paragraphs: [
				"Meu inicio como desenvolvedor foi veio muito antes do meu curso no senai. Quando ganhei meu primeiro celular aos 15 anos e comecei a navegar pela internet, sempre fui curioso sobre como os sites funcionavam e interagiam comigo, aquilo me enchiam os olhos.",
			]
		},
	},
	questions: {
		aboutMe: "Me fale mais sobre você",
		aboutExperience: "Me fale mais sobre sua experiência profissional.",
		aboutJobs: "Me fale sobre seus trabalhos ja feitos.",
		contact: "Como posso entrar em contato com você?",
		backToWelcome: [
			"Entendi, deixa eu perguntar sobre outras coisas...",
			"Muito bem, mas tem outra coisa que queria saber."
		]
	}
};

